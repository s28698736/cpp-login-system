#include <iostream>
#include <string>

int main()
{
	std::string username;
	std::string email;
	std::string password;
	std::cout << "What Is Your Username: ";
	getline (std::cin, username);
	std::cout << "What Is Your Email: ";
	getline (std::cin, email);
	std::cout << "What Is Your Password: ";
	getline (std::cin, password);
	std::cout << "Hello, " << username << "!\n";
	std::cout << "Your Email Is: " << email << "\n";
	std::cout << "Your Password Is: " << password;
}
